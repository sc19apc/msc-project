dlab_template('
	1-len:	[	
			object_size(c_length_type),
			overlap_with_target(c_overlap),
			object_direction_to_start(c_direction_start),
			object_direction_to_target(c_direction_target),
			surrounding_freespace(c_freespace),
			object_size(c_length_type),
			distance_from_object_to_start_ranking(c_ranking),
			distance_ranking_type(c_ranking_classification)
			
			
		]
	
<-- 
	0-len:[	
			distance_ranking_type(c_ranking_classification),
			object_size(c_length_type),
			object_direction_to_start(c_direction_start),
			object_direction_to_target(c_direction_target),
			surrounding_freespace(c_freespace),
			distance_from_object_to_start_ranking(c_ranking),
			overlap_with_target(c_overlap)
			
		]
').


dlab_variable(c_ranking_classification, 1-1,[close,near,far,too_far]).
dlab_variable(c_direction_start, 1-1,[n,nw,w,sw,s,se,e,ne]).
dlab_variable(c_direction_target, 1-1,[n,nw,w,sw,s,se,e,ne]).
dlab_variable(c_length_type, 1-2,[very_small_object,small_object,big_object]).
dlab_variable(c_overlap, 1-3,[very_low,low,high]).
dlab_variable(c_freespace, 1-3,[very_low,low,high]).
dlab_variable(c_ranking, 1-4,[1,2,3,4]).