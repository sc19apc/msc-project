dlab_template('
	0-len:	[	
			distance_from_gap_to_start_ranking(c_gap_ranking),
			distance_ranking_type(c_ranking_classification),
			gap_size(c_length_type),
			gap_direction_to_start(c_direction_start),
			gap_direction_to_target(c_direction_target)
		]
	
<-- 
	0-len:[	
			distance_from_gap_to_start_ranking(c_gap_ranking),
			distance_ranking_type(c_ranking_classification),
			gap_size(c_length_type),
			gap_direction_to_start(c_direction_start),
			gap_direction_to_target(c_direction_target)
			
		]
').


dlab_variable(c_ranking_classification, 1-1,[close,near,far,too_far]).
dlab_variable(c_gap_ranking, 1-len,[1,2,3,4]).
dlab_variable(c_direction_start, 1-1,[n,nw,w,sw,s,se,e,ne]).
dlab_variable(c_direction_target, 1-1,[n,nw,w,sw,s,se,e,ne]).
dlab_variable(c_length_type, 1-1,[very_small_gap,small_gap,big_gap,very_big_gap]).

