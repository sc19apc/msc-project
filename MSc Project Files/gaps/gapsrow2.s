use_package(icl).
use_package(query).
load(models).



classes([gap_selected, gap_not_selected]).

icl_multi(on).

significance_level(0.90).
talking(4).

simplify(on).


maxhead(10).
maxbody(10).
cv_sets(2).