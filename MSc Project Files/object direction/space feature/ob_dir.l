dlab_template('
	0-0:	[                  
			
			
		]
	
<-- 
	1-len:	[	
			ff_freespace(c_ranking_ff),
			bb_freespace(c_ranking_bb),
			ll_freespace(c_ranking_ll),
			rr_freespace(c_ranking_rr),
			fr_freespace(c_ranking_fr),
			fl_freespace(c_ranking_fl),
			br_freespace(c_ranking_br),
			bl_freespace(c_ranking_bl),			
			hand_to_object_direction_when_approaching_the_object(c_direction),
			target_to_object_direction(c_direction_target)
			
			
		] 
').

dlab_variable(c_ranking_rr, 1-1,[comfortable_fit,exact_fit,partial_fit,not_at_all_fit]).
dlab_variable(c_ranking_ff, 1-1,[comfortable_fit,exact_fit,partial_fit,not_at_all_fit]).
dlab_variable(c_ranking_bb, 1-1,[comfortable_fit,exact_fit,partial_fit,not_at_all_fit]).
dlab_variable(c_ranking_ll, 1-1,[comfortable_fit,exact_fit,partial_fit,not_at_all_fit]).
dlab_variable(c_ranking_br, 1-1,[comfortable_fit,exact_fit,partial_fit,not_at_all_fit]).
dlab_variable(c_ranking_bl, 1-1,[comfortable_fit,exact_fit,partial_fit,not_at_all_fit]).
dlab_variable(c_ranking_fl, 1-1,[comfortable_fit,exact_fit,partial_fit,not_at_all_fit]).
dlab_variable(c_ranking_fr, 1-1,[comfortable_fit,exact_fit,partial_fit,not_at_all_fit]).
dlab_variable(c_direction, 1-1,[fr,fl,br,bl]).
dlab_variable(c_direction_target, 1-1,[fr,fl]).


