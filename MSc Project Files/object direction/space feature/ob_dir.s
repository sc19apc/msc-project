use_package(icl).
use_package(query).
load(models).



classes([very_comfortable_fit_selected,comfortable_fit_selected,exact_fit_selected,partial_fit_selected,not_at_all_fit_selected]).

icl_multi(on).
multiclass(on).

significance_level(0.90).
talking(4).

simplify(on).


maxhead(20).
maxbody(20).
cv_sets(2).